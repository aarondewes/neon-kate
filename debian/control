Source: kate
Section: kde
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Modestas Vainius <modax@debian.org>,
           Sune Vuorela <sune@debian.org>,
           Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake (>= 2.8.12),
               debhelper (>= 11~),
               kf6-extra-cmake-modules,
               kf6-kactivities-dev,
               kf6-kconfig-dev,
               kf6-kcrash-dev,
               kf6-kdbusaddons-dev,
               kf6-kdoctools-dev,
               kf6-kguiaddons-dev,
               kf6-ki18n-dev,
               kf6-kiconthemes-dev,
               kf6-kitemmodels-dev,
               kf6-kjobwidgets-dev,
               kf6-kio-dev,
               kf6-knewstuff-dev,
               kf6-kparts-dev,
               kf6-plasma-framework-dev,
               kf6-kpty-dev,
               kf6-syntax-highlighting-dev,
               kf6-ktexteditor-dev,
               kf6-threadweaver-dev,
               kf6-kwallet-dev,
               kf6-kwindowsystem-dev,
               kf6-kxmlgui-dev,
               kf6-kcolorscheme-dev,
               kf6-kuserfeedback-dev,
               kf6-kmoretools-dev,
               pkg-config,
               pkg-kde-tools-neon,
               qt6-base-dev
Standards-Version: 4.2.1
Homepage: http://kate-editor.org/
Vcs-Browser: https://salsa.debian.org/qt-kde-team/kde/kate
Vcs-Git: https://salsa.debian.org/qt-kde-team/kde/kate.git

Package: kate
Section: editors
Architecture: any
Depends: kate6-data (>= ${source:Version}),
         ktexteditor-katepart,
         plasma-framework,
         qml-module-org-kde-kquickcontrolsaddons,
         qml-module-qtquick-layouts,
         qml-module-qtquick2,
         ${misc:Depends},
         ${shlibs:Depends}
Recommends: sonnet-plugins
Suggests: darcs,
          exuberant-ctags,
          git,
          khelpcenter,
          konsole-kpart,
          mercurial,
          subversion
Description: powerful text editor
 Kate is a powerful text editor that can open multiple files simultaneously.
 .
 With a built-in terminal, syntax highlighting, and tabbed sidebar, it performs
 as a lightweight but capable development environment.  Kate's many tools,
 plugins, and scripts make it highly customizable.
 .
 Kate's features include:
 .
  * Multiple saved sessions, each with numerous files
  * Scriptable syntax highlighting, indentation, and code-folding
  * Configurable templates and text snippets
  * Symbol viewers for C, C++, and Python
  * XML completion and validation

Package: kate6-data
Architecture: all
Depends: ${misc:Depends}
Replaces: kate (<< 4:19.04.0),
          kate-data (<< 4:4.14.3-0ubuntu4),
          kate5-data,
          ${kde-l10n:all}
Breaks: kate (<< 4:19.04.0), kate-data (<< 4:4.14.3-0ubuntu4), kate5-data, ${kde-l10n:all}
Description: shared data files for Kate text editor
 This package contains the architecture-independent shared data files needed
 for Kate editor

Package: kwrite
Section: editors
Architecture: any
Depends: ktexteditor-katepart, ${misc:Depends}, ${shlibs:Depends}
Breaks: kate5-data (<< 4:17.08.3-3~), ${kde-l10n:all}
Replaces: kate5-data (<< 4:17.08.3-3~), ${kde-l10n:all}
Description: simple text editor
 KWrite is a simple text editor built on the KDE Platform. It uses the Kate
 editor component, so it supports powerful features such as flexible syntax
 highlighting, automatic indentation, and numerous other text tools.

Package: ktexteditorpreviewplugin
Section: oldlibs
Architecture: all
Description: transitional package for KTextEditorPreviewPlugin
 Transitional package, can be safely removed.  The plugin is now part
 of Kate.
